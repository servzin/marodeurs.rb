# coding: utf-8
require 'telegram/bot'

TOKEN = ENV["MARODEURS_TELEGRAM_TOKEN"]
GODS_API_PATH = "https://godville.net/gods/api/"

def post_to_telegram(bot, message, text)
  bot.api.send_message(chat_id: message.chat.id, text: text)
end

def post_info(bot, message, response)
  body = JSON.parse(response.body)
  text = <<INFO
Имя Бога: #{body["godname"]}
Имя Героя: #{body["name"]}
Уровень: #{body["level"]}
Девиз: #{body["motto"]}
Кирпичей: #{body["bricks_cnt"]}
Брёвен: #{body["wood_cnt"]}
Сбережений: #{body["savings"]}
Питомец: #{body["pet"]["pet_class"]} #{body["pet"]["pet_name"]} #{body["pet"]["pet_level"]} уровня
Побед/поражений на арене: #{body["arena_won"]}/#{body["arena_lost"]}
INFO
  post_to_telegram(bot, message, text)
end

Telegram::Bot::Client.run(TOKEN, logger: Logger.new($stdout)) do |bot|
  bot.logger.info('Bot has been started. To arms, brothers!')
  bot.listen do |message|
    begin
      case message.text
      when '/start'
        post_to_telegram(bot, message, "Стой! Назови пароль!")
      when '/stop'
        post_to_telegram(bot, message, "Ну ладно.")
      when /\/info\s([^\A]+)/
        god_name = message.text.match(/\/info\s([^\A]+)\z/)[1].strip
        response = Faraday.get("#{GODS_API_PATH}#{URI.encode(god_name)}")
        if response.status == 200
          post_info(bot, message, response)
        else
          post_to_telegram(bot, message, "Не можем найти такого Бога. Может, он ещё не появился?")
        end
      else
        post_to_telegram(bot, message, "Повторяю: #{message.text}")
      end
    rescue => e
      bot.api.send_message(chat_id: message.chat.id, text: "Создатели! Меня тут пытаются положить, но я сопротивляюсь.")
    end
  end
end
